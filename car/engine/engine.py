from time import sleep
from adafruit_servokit import ServoKit


class Engine:

    """Engine module for the car."""

    def __init__(self, channel=1):
        """Initialize continuous servo.

        Parameters
        ----------
        channel: int, 0 <= channel <= 16
            Channel on the PCB.

        """
        kit = ServoKit(channels=16)
        self.continuous_servo = kit.continuous_servo[channel]
        self.continuous_servo.deinit()

    def get_throttle(self):
        """Get current throttle.

        Throttle is the value for velocity.
        The negative value for throttle is returned,
        because in this motor a negative value means
        the tires turn forward.

        Returns
        -------
        throttle: float
            Current velocity.

        """
        return -self.continuous_servo.throttle

    def set_throttle(self, throttle):
        """Set new throttle.

        A little helper function which calculates the
        negative value to set the correct throttle.
        See :Engine.get_throttle: for more information.

        Parameters
        ----------
        throttle: float, -1 < throttle < 1
            The throttle (velocity) to set the servo.

        """
        if throttle > 1:
            self.continuous_servo.throttle = -1
        elif throttle < -1:
            self.continuous_servo.throttle = 1
        else:
            self.continuous_servo.throttle = -throttle

    def accelerate(self, delta_v):
        """Accelerate the servo.

        Change the velocity of the servo by adding or subtracting
        a delta velocity.

        If the servo goes forward and the new velocity
        would be backwards, the servo has to be stopped completely
        for a short time (using :Engine.breaking:),
        due to reasons we don't understand.

        Parameters
        ----------
        delta_v: float

        """
        old_v = self.get_throttle()
        new_v = self.get_throttle() + delta_v
        if old_v > 0 and new_v < 0:
            self.breaking()
        self.set_throttle(new_v)

    def breaking(self, breakness=1):
        """Break the servo.

        If the servo was going forward,
        it needs to be stopped completely before changing direction.
        This is done using the :time.sleep: function from the
        python standard library.
        It sleeps the minimum required amount.

        Parameters
        ----------
        breakness: float, 0 < breakness < 1
            The force with which the servo will be stopped.

        """
        forward = self.get_throttle() > 0
        if forward:
            self.set_throttle(-breakness)
            sleep(0.02)
        else:
            self.set_throttle(breakness)
        self.continuous_servo.deinit()

    def close(self):
        """Close the continous servo.

        Set the servo speed to zero,
        to stop it completely when finishing
        a program.
        """
        self.continuous_servo.deinit()
