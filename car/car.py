from steering.steering import Steering
from engine.engine import Engine
from camera.camera import Camera
from controle.server import Socket
from controle.str_command_encoder import String_command_encoder
from threading import Thread


class Car(Steering, Engine, Socket, String_command_encoder, Camera):

    """The Car."""

    def __init__(self, cmd_dict):
        """Initialize the car.

        The car inherits all functions and variables
        directly from the :Steering: module and the :Engine: module.
        """
        Thread.__init__(self, daemon=True)
        Steering.__init__(self)
        Engine.__init__(self)
        Camera.__init__(self)
        Socket.__init__(self)
        String_command_encoder.__init__(self, cmd_dict)

    def run(self):
        """Start threads.

        Threads are:
        - socket
        - camera
        """
        self.tServer = Thread(target=self.listen_to_pilot)
        self.cServer = Thread(target=self.photo_series)
        self.tServer.start()
        self.cServer.start()

    def close(self):
        """Close Engine and Steering."""
        self.tServer.close()
        self.cServer.close()
        Engine.close(self)

    def information(self):
        """Print steering and throttle information into terminal."""
        print(
            "Steering:  {:2.4f}  |  Throttle:  {:2.4f}  ".format(
                self.get_angle(), self.get_throttle()
            ),
            end="\r",
        )
