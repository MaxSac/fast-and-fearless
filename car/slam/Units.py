import numpy as np

class UnitError(Exception):

    def __init__(self, message, errors=None):
        super().__init__(message)
        self.errors = errors

def to_degree(value, unit, exponent):
    if unit == 'deg':
        return value
    if unit == 'rad':
        return value * 180 / np.pi
    raise UnitError('Casting from {unit} to deg not possible')

def to_rad(value, unit, exponent):
    if unit == 'rad':
        return value
    if unit == 'deg':
        return value * np.pi/ 180
    raise UnitError('Casting from {unit} to deg not possible')

class Units:

    def __init__(self, value, unit, exponent=0):
        self.value = value
        self.unit = unit
        self.exponent = exponent

        self.switch = {
                'deg': to_degree,
                'rad': to_rad
                }

    def __call__(self, unit):
        return self.switch[unit](self.value, self.unit, self.exponent)

    def __add__(self, other):
        value_1 = self.switch[self.unit](self.value, self.unit, self.exponent)
        value_2 = self.switch[self.unit](other.value, other.unit, other.exponent)
        unit = self.unit
        exponent = 0
        return Units(value_1 + value_2, unit, exponent)

    def __sub__(self, other):
        value_1 = self.switch[self.unit](self.value, self.unit, self.exponent)
        value_2 = self.switch[self.unit](other.value, other.unit, other.exponent)
        unit = self.unit
        exponent = 0
        return Units(value_1 - value_2, unit, exponent)

    def __repr__(self):
        return "{} {}".format(self.value, self.unit)

    def __mul__(self, other):
        if isinstance(other, Units):
            value_1 = self.switch[self.unit](self.value,
                    self.unit, self.exponent)
            value_2 = self.switch[self.unit](other.value,
                    other.unit, other.exponent)
            unit = [self.unit, other.unit]
            exponent = 0
            return Units(value_1 * value_2, unit, exponent)
        else:
            return Units(self.value * other, self.unit, self.exponent)

    def __rmul__(self, other):
        if isinstance(other, Units):
            value_1 = self.switch[self.unit](self.value,
                    self.unit, self.exponent)
            value_2 = self.switch[self.unit](other.value,
                    other.unit, other.exponent)
            unit = [self.unit, other.unit]
            exponent = 0
            return Units(value_1 * value_2, unit, exponent)
        else:
            return Units(self.value * other, self.unit, self.exponent)

def main():

    arg1 = Units(90, 'deg')
    arg2 = Units(np.pi, 'rad')
    arg1 = arg1 + arg2
    print(1 * arg1('rad'))

if __name__== '__main__':
    main()
