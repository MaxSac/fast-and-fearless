import numpy as np
import pandas as pd
from Units import Units
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def lin(x, a, b):
    return a + b*x

class PosReco:

    def __init__(self, baseline):
        self.baseline = baseline

    def reconst_3D(self, parallaxe, beta):
        alpha_1 = beta + 2 * parallaxe
        alpha_2 = beta + parallaxe
        y = self.baseline * 0.5 / (np.tan(alpha_1('rad')) - np.tan(alpha_2('rad')))
        x = y*np.tan(alpha_2('rad'))
        return (x,y)

    def reconst_angle(self, x, y):
        parallaxe = Units(np.arctan((x+self.baseline/2) / y) - np.arctan(x/y), 'rad')
        beta = Units(np.arctan(x/y)-parallaxe('rad'), 'rad')
        return (parallaxe, beta)

    def fit(self, x, betas):
        self.pprm, self.pcov = curve_fit(lin, x, betas)


    def reconst_2D(self, x):
        return lin(x, *self.pprm)


def main():

    encoder = PosReco(1)

    print('============= Reconstruction Test ================')
    beta = Units(-45, 'deg')
    para = Units(45, 'deg')

    dx = 1
    dy = 5.2 

    p , b = encoder.reconst_angle(dx, dy)
    print('dx: ', dx, '\tdy: ', dy, '\tp: ', p('deg'), '\tb: ', b('deg'))

    print('\n================ Angular Fit ====================')
    df = pd.read_csv('../../picture/3D_cones.csv')

    rx = []
    rb = []
    for dx_i, dy_i, x_i in zip(df.dx.to_numpy()+0.5, df.dy.to_numpy(), df.x.to_numpy()):
        p, b = encoder.reconst_angle(dx_i, dy_i)
        print('dx: ', dx_i, '\tdy: ', dy_i , '\tp: ', p('deg'), '\tb: ',
                b('deg'), '\tx: ', x_i)
        rx.append(x_i)
        rb.append(b('deg'))

    encoder.fit(rx, rb)

    plt.plot(rx, rb, 'x')
    plt.plot(rx, lin(np.array(rx), *encoder.pprm))
    plt.savefig('disruption.pdf')

    print('\n================= 3D Check  =====================')

    df2 = df.iloc[[4,3], :]
    df2['dx'] += 0.5

    from time import time

    start = time()

    beta_r = Units(encoder.reconst_2D(df2['x'].iloc[0]), 'deg')
    beta_l = Units(encoder.reconst_2D(df2['x'].iloc[1]), 'deg')
    parallaxe = Units(beta_r('deg') - beta_l('deg'), 'deg') * 0.5
    x, y = encoder.reconst_3D(parallaxe, beta_l)

    end = time()
    print(f'Seconds for reconstruct pylons {(end - start) *1000} ms.')

    print("parallaxe: ", parallaxe)
    print("LEFT  dx: ", df2['dx'].iloc[0], 'dy: ', df['dy'].iloc[0], "beta: ", beta_l)
    print("RIGHT dx: ", df2['dx'].iloc[1], 'dy: ', df['dy'].iloc[1], "beta: ", beta_r)
    print('RECONSTRUCTED x: ', x, 'y: ', y)

if __name__== '__main__':
    main()
