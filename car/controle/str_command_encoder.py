class String_command_encoder:
    def __init__(self, str_com_dict, file_path="/tmp/serverdata.txt"):
        self.file_path = file_path
        self.str_com_dict = str_com_dict
        self.f = open(self.file_path, 'rb')

    def _encode(self):
        command = self.f.read().decode()
        return command.split('\n')[:-1]

    def _map_str_to_command(self):
        buffer_str = self._encode()
        
        for com in buffer_str:
            # print('decode: ', com)
            if com:
                if com in self.str_com_dict.keys():
                    eval(self.str_com_dict[com])


    def close(self):
        self.f.close()

def main():
    str_enc = String_command_encoder(
        {
            'FORWARD':"print('FORWARD')",
            'BACKWARD':"print('BACKWARD')",
            'RIGHT':"print('RIGHT')",
            'LEFT':"print('LEFT')",
        }
    )
    while 1:
        str_enc._map_str_to_command()

if __name__ == '__main__':
    main()
