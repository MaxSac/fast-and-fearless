import socket
import sys
from time import sleep, time


class Socket:
    def __init__(self, TCP_IP='0.0.0.0', TCP_PORT=5005, data_file="/tmp/serverdata.txt"):
        self.BUFFER_SIZE = 1024  # Normally 1024, but we want fast response
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.bind((TCP_IP, TCP_PORT))
        self.s.listen(3)

        self.socket_file = open(data_file, 'wb')

    def listen_to_pilot(self):
        while True:
            self.conn, self.addr = self.s.accept()
            print(self.conn)
            print(self.addr)
            print('accept conncetion')
            while True:
                data = self.conn.recv(self.BUFFER_SIZE)
                self.socket_file.write(data)
                self.socket_file.flush()
                if not data: break
                self.conn.send(data)  # echo

    def close(self):
        self.socket_file.close()
        try:
            self.conn.close()
        except AttributeError:
            pass
        self.s.close()
        print('Close connection')


def main():
    soc = Socket()
    try:
        soc.listen_to_pilot()
    except KeyboardInterrupt:
        soc.close()
    sys.exit(1)


if __name__ == '__main__':
    main()
