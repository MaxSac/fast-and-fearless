import numpy as np
from matplotlib import pyplot as plt
from time import sleep

a = np.fromfile("/tmp/serverdata.txt", dtype=np.uint8)
a = a.reshape((720, 1280, 3))

plt.imshow(a)
plt.show()
sleep(5)
