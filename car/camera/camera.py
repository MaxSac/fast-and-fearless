import cv2
from os import makedirs
from time import sleep


class CameraError(Exception):
    def __init__(self, msg, err):
        message = msg.format(**err)
        super().__init__(message)
        self.errors = err


class Camera:

    """Camera to take photos."""

    def __init__(self, video_input=0, resolution=(1280, 720)):
        # TODO: Make the path for the photos a parameter of the init function
        """Initialize a :cv2: camera.

        Create a directory for the photos.

        Parameters
        ----------
        video_input: int
            Integer describing the video input card used for photos.
            `0` is mostly the RaspberryPiCamera, and `2` is for USB Webcams.

        resolution: (int, int)
            Set the camera resolution.

        """
        self.cap = cv2.VideoCapture(video_input)
        self.video_input = video_input
        self._set_resolution(resolution)

        self.path = "/home/pi/Pictures/RPiCar"
        makedirs(self.path, exist_ok=True)
        self.picture_n = 1

    def _set_resolution(self, resolution):
        self.cap.set(3, resolution[0])
        self.cap.set(4, resolution[1])
        self.resolution = resolution

    def take_photo(self, binary=False):
        """Take a photo with the camera.

        Parameters
        ----------
        binary: bool, False
            If False, return a numpy array, else return the byte string

        Returns
        -------
        frame: numpy.ndarray / bytestring
            See parameter `binary`.

        """
        ret, frame = self.cap.read()

        if not ret:
            raise CameraError(
                "No photo taken by camera {video_input} with resolution {resolution}",
                dict(video_input=self.video_input, resolution=self.resolution),
            )

        if binary:
            frame = frame.tobytes()

        return frame

    def _save_photo(self, filename, frame):
        cv2.imwrite(filename, frame)

    def photo_series(self, sleep_time=5):
        """Take may photos and save them to the directory.

        Parameters
        ----------
        sleep_time: int
            Time to sleep in seconds.

        """
        while True:
            frame = self.take_photo()
            self._save_photo(
                self.path + "/{}.png".format(self.picture_n), frame
            )
            self.picture_n += 1
            sleep(sleep_time)

    def close(self):
        """Close camera."""
        pass


# class CameraStream:
#     def __init__(self, camera_list, tcp_ip):
#         self.cameras = camera_list
#         self.client = Client()
#         self.client.TCP_PORT = 5005
#         self.client.TCP_IP = tcp_ip

#     def stream(self):
#         for cam in self.cameras:
#             byteframe = cam.take_photo(binary=True)
#             self.client.send_data(byteframe)
#             self.client.close()


# def try_photo():
#     c = Camera()
#     c.photo_series()


# def try_stream():
#     from time import sleep

#     cam = Camera(resolution=(640, 480))
#     stream = CameraStream([cam], "192.168.4.8")
#     stream.stream()
#     sleep(10)


# def main():
#     try_photo()
#     # try_stream()


# if __name__ == "__main__":
#     main()
