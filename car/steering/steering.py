from adafruit_servokit import ServoKit


class Steering:

    """Steering module for the car."""

    def __init__(self, channel=0):
        """Initialize servo.

        Set minimum and maximum parameters
        for leftmost and rightmost steering angles.
        Set mid parameter for foreward driving.

        Parameters
        ----------
        channel: int, 0 <= channel < 16
            Channel on the PCB.

        """
        kit = ServoKit(channels=16)
        self.servo = kit.servo[channel]
        self.servo.actuation_range = 180
        self.servo_min = 40
        self.servo_max = 160
        self.servo_mid = 100
        self.servo.angle = self.servo_mid

    def change_angle(self, angle=0):
        """Change the steering angle.

        Add the new angle to the current angle.
        If the new angle is above/below the max/min parameters,
        it is set to the max/min value.

        Parameters
        ----------
        angle: int
            Relative steering angle

        """
        new = self.servo.angle + angle
        if new > self.servo_min and new < self.servo_max:
            self.servo.angle = new
        elif new < self.servo_min:
            self.servo.angle = self.servo_min
        elif new > self.servo_max:
            self.servo.angle = self.servo_max

    def get_angle(self):
        """Get current steering angle.

        An angle which is 0 means the car goes straight.

        Returns
        -------
        angle: int
            Current angle, with 0 if car goes straight.

        """
        return self.servo.angle - self.servo_mid

    def close(self):
        """Close the servo.

        Set the servo angle to mid.
        """
        self.servo.angle = self.servo_mid
