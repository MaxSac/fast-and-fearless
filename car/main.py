from car import Car
import sys


def main():
    """Control the car from a computer."""
    cmd_dict = {
        "FORWARD": "self.accelerate(0.02)",
        "BACKWARD": "self.accelerate(-0.02)",
        "LEFT": "self.change_angle(-8)",
        "RIGHT": "self.change_angle(8)",
        "CLOSE": "self.close()",
    }

    car = Car(cmd_dict)
    car.run()
    try:
        while 1:
            car._map_str_to_command()
            car.information()
    except KeyboardInterrupt:
        car.close()
        sys.exit()


if __name__ == "__main__":
    main()
