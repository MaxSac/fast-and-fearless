# Technical Requirements

Thinks you need to build a rc-car which is controlled by a raspberry.

## Communication Side

[Raspberry Pi 3](https://www.amazon.de/Raspberry-1373331-Pi-Modell-Mainboard/dp/B07BDR5PDW/)
[Adafruit PWM Servo Driver](https://www.amazon.de/Adafruit-12-Channel-16-bit-PWM-Driver/dp/B00SLYAYU8/) 10.77
[Webcam](https://www.amazon.de/Logitech-222A083-C170-Webcam-schwarz/dp/B01BGBJ8XQ) 20-30€

## Hardware Side

[chassis](https://www.amazon.de/Reely-10-Elektro-ONROAD-Chassis-ARR/dp/B01FO41EVK/) 50.00 €
[wheels](https://www.amazon.de/BQLZR-Y-Form-Felge-Reifen-Road/dp/B00ID51PT4/) 5.00 €
[Motor with speedcontroler](https://www.amazon.de/Crazepony-UK-Sensorless-Brushless-Electronic-Controller/dp/B07C2J5RMT/) 30 €
[Motor](https://www.amazon.de/Crazepony-UK-3100KV-Brushless-Waterproof-3-175mm/dp/B07B3WD638/) 20 €
[ESC](https://www.amazon.de/PIXNOR-Brushless-Fahrtenregler-Quad-Rotor-X-Copter-Wie-gezeigt/dp/B01DZQC404/) 9€
[Servo](https://www.amazon.de/Micro-Servo-High-Torque-Metal/dp/B00KLBTWGG/) 11 €
[Akku](https://www.amazon.de/Kraftmax-Hochleistungs-Akku-mit-Tamiya-Stecker/dp/B00GOWVBU6/) 30 €
[T-Plug](https://www.amazon.de/Carson-500906105-Adapter-T-Plug-Tamiya/dp/B009OY1IIG/) 5€

#Summasumarum 
201 €
