import numpy as np
import pandas as pd
from glob import glob

x, y, w, h, dx, dy = np.genfromtxt("cam_pos.txt", unpack=True)

pictures = sorted(glob("*.JPG"))
filename = []
for i in range(0,3*(len(pictures)-1)):
    filename.append(pictures[int(i/3)])

pic = {'x':[], 'y':[], 'w':[], 'h':[], 'dx':[], 'dy':[], 'filename':[]}

for x_i, y_i, w_i, h_i, dx_i, dy_i, filename_i in zip(x,y,w,h,dx,dy,filename):
    if (h_i < 0) or (w_i < 0):
        continue
    pic['x'].append(x_i)
    pic['y'].append(y_i)
    pic['w'].append(w_i)
    pic['h'].append(h_i)
    pic['dx'].append(dx_i)
    pic['dy'].append(dy_i)
    pic['filename'].append(filename_i)

df = pd.DataFrame(pic)
df.to_csv('3D_cones.csv', index=False)
