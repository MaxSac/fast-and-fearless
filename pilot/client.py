from threading import Thread
import socket
from time import sleep

class Client(Thread):
    def __init__(self, TCP_IP="0.0.0.0", TCP_PORT=5005):
        ''' Start a TCP-Client at localhost. Bind connections
        and set timeout to 1 sec, to raise Error if connection is
        lost.
        Parameters:
            TCP_IP: str, "0.0.0.0"
                four values smaller than 256 from the server
                to connect. default value is set to localhost
            TCP_PORT: int, 5005
                port with server could be reached. Have to be
                over 2000, to prevent communication with kernel
                sockets.
        '''
        self.TCP_IP = TCP_IP
        self.TCP_PORT = TCP_PORT
        # self.BUFFER_SIZE = 1024
        self.BUFFER_SIZE = 2
        self.timeout = 1
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((self.TCP_IP, self.TCP_PORT))
        self.s.settimeout(self.timeout)

    def send_data(self, msg):
        ''' Send binary data to socket and wait till socket response.
        msg: b'str
            String which has to be transfered
        '''
        self.s.send(msg)
        data = self.s.recv(self.BUFFER_SIZE)

    def close(self):
        ''' Close socket '''
        self.s.close()

def test_message():
    cli = Client()
    mes = input('Your Message: ').encode()
    cli.send_data(mes)
    cli.close()

def test_picture():
    from PIL import Image
    picture = open("/home/maximilian/Pictures/00-wallpaper/rocks.jpg",
            'rb').read()
    cli = Client()
    cli.send_data(picture)
    cli.close()

if __name__ == '__main__':
    test_message()
    # test_picture()
