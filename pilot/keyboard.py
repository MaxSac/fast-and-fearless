from pynput import keyboard
from threading import Thread
from time import sleep
import sys

class Keyboard(Thread):
    def __init__(self):
        '''Initalize a keyboard at a independent Thread.'''
        Thread.__init__(self, daemon=True)
        self.daemon = True
        self.start()
        self.key_events = dict()
        self.key_function = dict()

    def on_press(self, key):
        ''' create a key in dict with key.char and set to pressed
        Parameters:
            key: keyboard.key
                Parameter from keyboard Listener
        '''
        try:
            self.key_events[key.char] = 'pressed'
        except AttributeError:
            self.key_events[key] = 'pressed'

    def on_release(self, key):
        ''' delete a key in dict with key.char and set to pressed
        Parameters:
            key: keyboard.key
                Parameter from keyboard Listener
        '''
        try:
            self.key_events.pop(key.char, None)
        except AttributeError:
            self.key_events.pop(key, None)

    def get_keys(self):
        ''' Return a list with keyboard.key.chars '''
        return [*self.key_events]

    def run(self):
        ''' Run a thread which try to get keypresses '''
        with keyboard.Listener(on_press=self.on_press,
                on_release=self.on_release,
                suppress=True
                ) as listener:
            try:
                listener.join()
            finally:
                listener.stop()
        sys.exit(1)

    def set_functions(self, key_fuctions):
        ''' Set a dict which map keys to functions/strings
        Parameters:
            key_function: dict
                dict_keys with keyboard keys and commands to encode
        '''
        self.key_fuctions = key_fuctions

    def encode_key(self, keys):
        ''' map keys to function
        Parameters:
            key: keyboard.key
                Parameter from keyboard Listener
        Return:
            endcoded_function: str
                functions in form of a string
        '''
        try:
            return [self.key_fuctions[key] for key in keys]
        except KeyError:
            print("{} not in key_function dict or dict not set.".format(keys))

def main():
    key = Keyboard()
    while True:
        actuel_key = key.get_keys()
        if 'q' in actuel_key:
            sys.exit(0)
        print(actuel_key)
        sleep(0.5)

if __name__ == '__main__':
    main()
