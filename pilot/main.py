from client import Client
from keyboard import Keyboard
from time import sleep


keys = {
    'w': b'FORWARD\n',
    's': b'BACKWARD\n',
    'a': b'LEFT\n',
    'd': b'RIGHT\n',
}


def main(verbose=False):
    cli = Client(TCP_IP = '192.168.4.1')
    kybd = Keyboard()
    while True:
        actuel_key = kybd.get_keys()
        if not actuel_key:
            continue
        if 'q' in actuel_key:
            raise KeyboardInterrupt
        kybd.set_functions(keys)
        cmds = kybd.encode_key(actuel_key)
        if cmds is not None:
            for cmd in cmds:
                cli.send_data(cmd)
        sleep(0.1)


if __name__ == '__main__':
    main(verbose=True)
