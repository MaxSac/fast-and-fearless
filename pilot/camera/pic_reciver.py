from server import Socket
from time import sleep

def main():
  soc = Socket('/tmp/photo')
  soc.start()
  try:
    while 1: sleep(10)
  except KeyboardInterrupt:
    soc.close()
  sys.exit(1)
   
if __name__ == '__main__':
  main()
