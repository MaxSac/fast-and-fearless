install: install_script.sh .env 
	bash $<

.env:
	python3 -m venv $@

.PHONY: install
