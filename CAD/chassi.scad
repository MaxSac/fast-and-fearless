chassis_points = [
    [  0,  0],  
    [130,  0],
    [155, 45],
    [155, 85],
    [130,130],
    [  0,130],
    [-65, 95],
    [-65, 35],
  ];

drive_shaft_points = [
    [0,0],
    [0,27],
    [3,30],
    [12,30],
    [15,27],
    [15,0],
   ];



/* union(){ */
module drive_shaft() {
    rotate([90,0,90])
    translate([57.5,0,45])
    linear_extrude(height = 220, center = true)
    polygon(drive_shaft_points, center=true);

    translate([-45,65,30])
    difference(){
    cylinder(10,6,6);
    cylinder(10,3,3);
    }

    translate([100,65,30])
    difference(){
    cylinder(10,6,6);
    cylinder(10,3,3);
    }

}

module chassi(){
difference()
    {
        linear_extrude(height = 15, center = true)
        polygon(chassis_points, convexity=5, center=true);

        translate([0,0,3])
        linear_extrude(height = 10, center = true)
        offset(r=-5){
            polygon(chassis_points, convexity=5, center=true);
        }
    }
}



chassi();
drive_shaft();
