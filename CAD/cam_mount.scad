include <drive_shaft.scad>

module cam_fixer() {
    cube([0.8,0.8,60], center=true);
}

module cam_mount() {
    translate([0,0,-5]) drive_shaft();
    translate([0,0, 5]) drive_shaft();
    translate([2,drill_versatz,0.5]) cube([0.8,0.8,11], center=true);
    translate([-0.65,drill_versatz,0.5]) cube([0.8,0.8,11], center=true);
    translate([2,-drill_versatz,0.5]) cube([0.8,0.8,11], center=true);
    translate([-0.65,-drill_versatz,0.5]) cube([0.8,0.8,11], center=true);

    /* translate([6.4,0,0]) cam_fixer(); */
}

cam_mount();
