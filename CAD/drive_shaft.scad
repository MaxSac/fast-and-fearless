
hoehe = 5;
seitenlaenge = 10;
frame_width = 2.0;
drill_versatz = seitenlaenge/2 - frame_width/2;
drill_bias = 0.5;

drill=0.8;
translation = 9;

module drive_form() {
 square([hoehe, seitenlaenge], center=true);
 translate([hoehe/2,0,0])
     circle(d=seitenlaenge);
}

module cam_fixpoint() {
    rotate([0,90,0])
    cylinder(6,0.2, 0.2, center=true);
}

module drill(x, y) {
    translate([x,y,0])
        square(drill, center=true);
}

module drive_shaft() {
    difference() {
        linear_extrude(1){
            difference() {
                difference(){
                    drive_form();
                    offset(delta=-frame_width){
                        drive_form();
                    }
                }
                translate([drill_bias,0,0])
                union(){
                    drill(1.5   ,drill_versatz);
                    drill(-1.15 ,drill_versatz);
                    drill(1.5   ,-drill_versatz);
                    drill(-1.15,-drill_versatz);
                }
                drill(6.4,0);
            }
        }
        translate([9,0.0,0.5])
            cam_fixpoint();
    }
}
